import com.javacorepractice.models.User;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


import java.util.*;

/**
 * @author m.shevchenko on Jul 2022
 */
public class CollectionsTopicTest {
    private User user1;
    private User user2;
    private User user3;
    private User user4;
    private User user5;
    private User user10;
    private User user11;
    private List<User> usersList;
    private List<User> usersList1;

    @BeforeClass
    public void setUp() {
        user1 = new User("adams1", "9283r7h2i3r");
        user2 = new User("fbrynn", "a1fe5f135");
        user3 = new User("cheney", "fq3fa5efe1");
        user4 = new User("faulkner", "qwrety346");
        user5 = new User("hillard3", "f54q63fee");

        user10 = new User("adams1", "4572i3r");
        user11 = new User("adams1", "9283r7h2i3r");

        usersList = new ArrayList<>();
        usersList.add(user2);
        usersList.add(user3);
        usersList.add(user4);
        usersList.add(user5);

        usersList1 = new ArrayList<>();
        usersList1.add(user1);
        usersList1.add(user2);
        usersList1.add(user3);
        usersList1.add(user4);
        usersList1.add(user5);
        usersList1.add(user10);
        usersList1.add(user11);
    }


    @Test(description = "Sorted list task output")
    public void listSortingTest() {
        List<User> userListSortedByUsername = new LinkedList<>(usersList1);
        System.out.println("List before sorting: " + userListSortedByUsername);

        Collections.sort(userListSortedByUsername);
        ListIterator<User> listIterator = userListSortedByUsername.listIterator();
        while (listIterator.hasNext()) {
            System.out.println("iterator = " + listIterator.next());
        }

        List<User> userListSortedByPassword = new LinkedList<>(usersList1);
        System.out.println("List before sorting: " + userListSortedByPassword);

        Collections.sort(userListSortedByPassword, Comparator.comparing(User::getPassword));
        ListIterator<User> listIterator1 = userListSortedByPassword.listIterator();
        while (listIterator1.hasNext()) {
            System.out.println("iterator = " + listIterator1.next());
        }
    }

    @Test(description = "Sorted sets creation task output")
    public void sortedSetCreationMethod() {
        Set<User> userSortedByPasswordSet = new TreeSet<>(Comparator.comparing(User::getPassword));
        userSortedByPasswordSet.addAll(usersList);
        System.out.println("Sorted Set by password: " + userSortedByPasswordSet);
        Set<User> userSortedByUsernameSet = new TreeSet<>();
        userSortedByUsernameSet.addAll(usersList);
        System.out.println("Sorted Set by username: " + userSortedByUsernameSet);
    }

    @Test(description = "Set elements adding task output")
    public void setElementsAddingMethod() {
        Set<User> userSet = new HashSet<>(usersList);
        System.out.println(userSet);
        System.out.println("user1 hash: " + user1.hashCode());
        userSet.add(user1);
        System.out.println(userSet);
        System.out.println("user10 hash: " + user10.hashCode());
        userSet.add(user10);
        System.out.println(userSet);
        System.out.println("user11 hash: " + user11.hashCode());
        boolean result = userSet.add(user11);
        if (!result) {
            System.out.println("user11 doesn't added to Set");
        }
        System.out.println(userSet);
    }

    @Test(description = "Set iterator task output")
    public void setIteratorMethod() {
        Set<User> userSet = new HashSet<>();
        userSet.add(user1);
        userSet.add(user2);
        userSet.add(user3);
        userSet.add(user4);
        userSet.add(user5);

        Iterator<User> iterator = userSet.iterator();
        while (iterator.hasNext()) {
            System.out.println("iterator = " + iterator.next());
        }
    }

}
